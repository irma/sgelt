# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.9.0] - 2024-11-01

### Changed

-  Use mdslicer library for markdown parsing (#20)

## [4.8.2] - 2024-09-27

### Fixed

- Fix locale error if not installed (#15)

### Changed

- Use tag_message for release notes
- Publish to PyPI instead of GitLab package registry (#18)

### Added

- Test with Python 3.12

## [4.8.0] - 2023-08-13

### Changed

- Use parallel:matrix jobs in CI

### Added

- Deploy packages to gitlab package registry 

## [4.7.0] - 2023-08-30

### Added

- Do not exclude events whose title is TBA
- Exclude events whose speaker is TBA

## [4.6.3] - 2023-05-16

### Fixed

- Summary for ical events

## [4.6.2] - 2023-05-16

### Fixed

- Error in ical event datetime for conferences

## [4.6.1] - 2023-05-16

### Fixed

- Error in ical generation if `speaker` is not defined in a conference program entry

## [4.6.0] - 2023-05-16

### Added

- ical link for conferences

## [4.5.0] - 2023-04-28

### Added

- Hover effect on conference program entry

### Changed

- Footer in French

## [4.4.0] - 2023-02-24

### Changed

- faster CI

## [4.3.4] - 2023-02-24

### Fixed

- CI error with python 3.10
- Restore release job involuntarily removed

## [4.3.3] - 2023-02-23

### Changed

- Little enhancement on accessibility

## [4.3.2] - 2023-02-21

### Changed

- Minor changes in package (package-data strategy)

## [4.3.0] - 2023-02-21

### Changed

- Package with pyproject.toml instead of setup.cfg
- Use tox for testing, linting and coverage

## [4.2.0] - 2023-02-15

### Changed

- Better accessibility for screen readers

## [4.1.1] - 2023-02-15

### Changed

Use `datetimejson` external package to handle datetime in JSON.

## [4.1.0] - 2023-02-10

### Added

- Enhance accessibility for screen readers

## [4.0.0] - 2023-02-09

### Changed

- "Agenda" rubrique is replaced by "À venir".
- Many font icons are replaced by SVG icons.

## [3.0.1] - 2022-11-21

### Fixed

- Temporary fix for the issue with icalendar >= 5

## [3.0.0] - 2022-11-21

### Added

- Added a parameter to handle then maximum number of news items to be displayed on the homepage.

## [2.3.2] - 2022-10-25

### Fixed

- Unfinished events must be displayed on homepage.

## [2.3.1] - 2022-10-21

### Added

- Handle start/end dates for events.

## [2.3.0] - 2022-09-28

### Added

- Handle partners logos.

## [2.2.6] - 2022-09-28

### Fixed

- Include "invité" status on team pages.
- Fix overflowing text on home page cards.

## [2.2.5] - 2022-09-19

### Fixed

- Missing colloquium indexation.

## [2.2.4] - 2022-09-02

### Added

- Better handle markdown parsing errors.

## [2.2.3] - 2022-08-30

### Fixed

- Fix ToC slugifier

## [2.2.2] - 2022-08-30

### Fixed

- Fix broken links: `actualités*.html` -> `actualites.htm`

## [2.2.1] - 2022-08-26

### Added

- Better frontmatter parsing error handling for markdown

## [2.1.1] - 2022-08-24

### Fixed

- Missing timezone in .ics files leading to wrong Google Calendar imports

## [2.1.0] - 2022-08-19

### Added

- Publish fake site with GitLab Pages

## [2.0.0] - 2022-08-19

### Changed

- HTML index files are now in event categories subdir
- use plural for event category URLs

## [1.0.0] - 2022-08-18

### Changed

- All html paths are now slugified.

### Fixed

- Fixed tab error for "groupe de travail" events.
