"""Enable to run with `python -m sgelt`"""

from .main import main

if __name__ == '__main__':
    main()
