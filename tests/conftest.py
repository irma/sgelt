import pytest
try:
    from pytest_cov.embed import cleanup_on_sigterm
except ImportError:
    pass
else:
    cleanup_on_sigterm()

from sgelt import config, sgelt
from common import test_path, test_content_path


@pytest.fixture(scope='session', autouse=True)
def faker_session_locale():
    return ['fr_FR']


@pytest.fixture(scope='session', autouse=True)
def faker_seed():
    return 4321


@pytest.fixture
def miniwebsite(tmp_path):
    """A fixture that creates and resets a Website object"""
    # Read config from tests/config.yml file
    conf = config.Config(config_path=test_path / 'config.yml')
    conf.load()
    # Override tests/config.yml with the following values:
    conf.update(content_path=test_content_path,
                theme_path=test_path / conf.theme_path,
                output_path=tmp_path / "output",
                json_agenda_path=test_path / "fake_agenda.json",
                json_teams_path=test_path / "fake_teams.json")
    website = sgelt.Website(conf)
    yield website
