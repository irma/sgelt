---
title: Mentions légales
---

## Ecriture et publication

Directeur de la publication : Bilbon

Éditeurs :

- Frodon
- Peter Doe

## Hébergement et contact

- Adresse de contact : [reachme@reach.me](mailto:reachme@reach.me)
- Hébergement Web : Shelob
- Entretien : Gollum

## Création de sites

- Sauron
- Mordor

## Contenu

### Graphiques

La conception graphique repose sur [Materialize](https://materializecss.com/).

### Les textes

Tout le monde peut les lire.

### Des photos

Tout le monde peut les regarder.

## Tes droits

Pour toute information ou exercice de vos droits sur le traitement des données, vous pouvez contacter [reachme@reach.me](mailto:reachme@reach.me).
