---
title: Informations pratiques
menu_item: Bibliothèque
category: bib
---

## Inscriptions

Aut dolorem aperiam sit harum dolores quo dolor temporibus. Eos distinctio animi et soluta architecto vel dolores libero 33 quasi quia sed dicta sapiente est ratione doloribus. Et sequi quibusdam ex impedit deleniti sit dolorem reiciendis. Est similique rerum id nobis amet est facere explicabo est modi dolorem et dicta dolorum aut expedita dolore est praesentium eius.

## Horaires

Lorem ipsum dolor sit amet. Et doloribus facere ut galisum earum ut molestiae nihil est dolorem illum quo nihil accusamus vel velit eligendi. Sit porro sunt hic fugit fuga non quisquam sapiente ab ducimus eligendi. Et ipsum earum sed omnis asperiores et amet ducimus. Sed enim nobis et quibusdam quos qui consequatur consequatur.

Sit aliquid voluptatem eum voluptas deserunt eum cupiditate sunt id doloremque iure ut Quis dolor aut quidem natus non labore quae. Ut voluptatum molestiae in adipisci labore et nobis neque ad excepturi quas eum ipsam odio?

Aut dolorum tenetur sit rerum iusto ut quia perferendis eos nihil voluptatibus non reprehenderit galisum. Ut dolores asperiores et fugiat sunt sit itaque quia quo ratione deleniti. 33 harum dignissimos et iste natus et repudiandae dolores nam architecto quasi aut voluptate rerum sit sint laborum.

## Équipe

* Scientific leader: Jane Doe
* Communication: Jean Dupont
