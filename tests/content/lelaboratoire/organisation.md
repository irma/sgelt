---
title: Organisation
menu_item: Le laboratoire
---

## Responsabilités

- ouvrier agricole : Sophie-Luce Perez
- étanchéiste : Inès Leroy
- psychanalyste : Alix Ribeiro
- danseur : Alexandre Rodrigues
- vendeur-magasinier en fournitures automobiles : Alfred Guillaume
- zoologiste : Thibaut Carre

## Organigramme

![organigramme](img/fake.png){: .materialboxed .responsive-img}

{{% button href="attachments/fake.pdf" icon="cloud_download" text="version PDF" %}}
