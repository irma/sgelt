---
title: Wonder Great
menu_item: Teams
category: team
team: WG
---

## Membres

### Membres permanents

{{% team_members permanent=True %}}

### Membres non permanents

{{% team_members permanent=False %}}

## Activités

{{% team_seminars %}}
