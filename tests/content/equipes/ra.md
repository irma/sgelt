---
title: Really Amazing
menu_item: Teams
category: team
team: RA
---

## Membres

### Membres permanents

{{% team_members permanent=True %}}

### Membres non permanents

{{% team_members permanent=False %}}

## Activités

{{% team_seminars %}}